import { Routes, Route } from "react-router-dom";
import axios from "axios";
import { useState, useEffect } from "react";
import { useCookies } from "react-cookie";

import MainNavBar from "./components/MainNavBar";

import RootView from "./components/RootView";
import DashBoard from "./components/Dashboard";
import MyProfile from "./components/MyProfile";

export default function App() {
  const [cookies, setCookie] = useCookies(["ANIPBtoken", "ANIPBid"]);
  const [user, setUser] = useState({});
  const [isSigned, handleSignedStatus] = useState(false);
  const [isVerified, handleVerifiedStatus] = useState(false);
  const [hasToken, handleTokenStatus] = useState(false);
  const [disclaimerReaded, handleDisclaimerStatus] = useState(false);

  const handleCookies = (id, token) => {
    if (token && token !== "null") {
      setCookie("ANIPBtoken", token, { path: "/" });
    }
    setCookie("ANIPBid", id, { path: "/" });
  };

  const handleVerification = (data) => {
    setCookie("ANIPBtoken", data.token, { path: "/" });
  };

  const handleRoleAssign = (data) => {
    setUser(data.user);
  };

  const handleDisclaimer = data => {
    setUser(data.user);
  }

  const loadUserData = async (id, token) => {
    const config = {
      headers: { "Content-Type": "application/json" },
    };
    const response = await axios.get(
      `http://localhost:5010/api/userById/${id}`,
      config
    );
    if (response.data) {
      setUser(response.data);
    }
  };

  useEffect(() => {
    if (user._id) {
      handleSignedStatus(true);
      handleVerifiedStatus(user.verified);
      handleDisclaimerStatus(user.disclaimerReaded);
    }
  }, [user]);

  useEffect(() => {
    if (cookies.ANIPBid) {
      loadUserData(cookies.ANIPBid);
      if (cookies.ANIPBtoken && cookies.ANIPBtoken !== "null") {
        handleTokenStatus(true);
      }
    }
  }, [cookies]);

  return (
    <div className="page-wrapper">
      <div className="blank-menu">
        <div className="menu-item-background">
          <img className="menu-logo" src="rpbw.png"></img>
        </div>

        <MainNavBar role={user.role}></MainNavBar>
      </div>
      <div className="page-content">
        <Routes>
          <Route
            exact
            path="/"
            element={
              <RootView
                isSigned={isSigned}
                isVerified={isVerified}
                userId={user._id}
                token={cookies.ANIPBtoken}
                disclaimerReaded={disclaimerReaded}
                onSignUp={handleCookies}
                onDisclaimerReaded={handleDisclaimer}
              />
            }
          />
          <Route
            path="/my-profile"
            element={
              <MyProfile
                user={user}
                onVerify={handleVerification}
                token={cookies.ANIPBtoken}
                onRoleAssign={handleRoleAssign}
              />
            }
          />
          <Route path="/dash" element={<DashBoard user={user} />} />
        </Routes>
        {hasToken && disclaimerReaded && <h1>Dashboard</h1>}
      </div>
    </div>
  );
}
