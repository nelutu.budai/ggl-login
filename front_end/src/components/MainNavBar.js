import { Icon } from "@mui/material";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import "./MainNavBar.css"

const navData = [
  {
    role: undefined,
    items: [
      {
        icon: "privacy_tip",
        text: "Termeni si conditii",
        link: "/terms",
      },
      {
        icon: "vpn_lock",
        text: "Politica GDPR",
        link: "/gdpr",
      },
    ],
  },
];

const MainNavBar = ({ role }) => {
  const [activeMenu, setActiveMenu] = useState([]);
  useEffect(() => {
    setActiveMenu(
      navData.find((item) => item.role === role) ?
        navData.find((item) => item.role === role).items:[]
    );
  }, [role]);

  return (
    <>
      {activeMenu.map((item, index) => (
        <Link
          key={`link-${index}`}
          to={item.link}
          className="menu-item-wrapper"
          style={{textDecoration: "none"}}
        >
          <div className="link-wrapper">
            <Icon sx={{ fontSize: 50 }}>{item.icon}</Icon>
            {item.text}
          </div>
        </Link>
      ))}
    </>
  );
};

export default MainNavBar;
