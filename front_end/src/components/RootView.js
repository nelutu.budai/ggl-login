import NotLogged from "./RootViews/NotLogged";
import NotVerified from "./RootViews/NotVerified";
import DisclaimerBox from "./RootViews/DisclaimerBox";


const RootView = ({ userId, token, isSigned, isVerified, disclaimerReaded, onSignUp, onDisclaimerReaded }) => {

    return (
        <>
            {!isSigned && <NotLogged onSignUp={onSignUp}/>}
            { isSigned&&!isVerified && <NotVerified/> }
            { isVerified&&!disclaimerReaded && <DisclaimerBox userId={userId} token={token} onDisclaimerReaded={onDisclaimerReaded}/> }
        </>
    )
}

export default RootView