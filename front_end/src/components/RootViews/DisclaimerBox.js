import { Button } from "@mui/material";
import "./DisclaimerBox.css";

import axios from "axios";


const DisclaimerBox = ({userId, token, onDisclaimerReaded }) => {


    const handleDisclaimer = async () =>{

        const response = await axios({
            method: "patch",
            url: `http://localhost:5010/api/disclaimer-readed/${userId}`,
            data: {
              disclaimerReaded: true,
            },
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${token}`,
            },
          });
          onDisclaimerReaded(response.data);

    }


  return (
    <div className="static-dialog-with-overflow">
      <img className="dialog-logo" src="/rpb.png"></img>
      <h3>Citeste cu atentie urmatorul disclaimer</h3>

      <p>
        Cillum aliqua occaecat voluptate ea nostrud proident minim eiusmod
        reprehenderit aliquip eu proident et. Consectetur reprehenderit aute sit
        enim. Esse ea ad ex irure enim. Est id aute qui eiusmod elit nulla
        commodo velit fugiat sunt nostrud proident labore consequat.
      </p>

      <p>
        Sunt labore dolor est ut irure duis ullamco pariatur aliquip deserunt
        adipisicing enim commodo. Cupidatat dolore proident voluptate dolore
        velit anim aute nulla labore qui dolore. Voluptate minim est commodo est
        excepteur reprehenderit reprehenderit labore excepteur reprehenderit
        veniam fugiat dolore. Est adipisicing id cillum fugiat officia. Labore
        quis nisi eiusmod cupidatat et laborum irure ut deserunt eiusmod mollit
        ullamco. Consectetur ea deserunt labore elit. In magna ex dolor est
        occaecat culpa cupidatat proident in.
      </p>

      <p>
        Reprehenderit fugiat esse cupidatat est est do. Anim proident aute magna
        ad pariatur aute voluptate. Deserunt mollit mollit do non aliquip
        consequat.
      </p>

      <p>
        Mollit non eu eu deserunt enim ullamco pariatur. Lorem proident sunt
        tempor non tempor cillum nostrud enim ipsum. Do ipsum ipsum irure amet
        nisi ex exercitation culpa exercitation consectetur laborum. Duis et ex
        ad consequat nostrud nisi id tempor ea. Adipisicing Lorem officia est
        exercitation officia irure sit sit veniam ut sint proident. Lorem ipsum
        labore tempor reprehenderit nostrud.
      </p>

      <p>
        Officia quis duis labore sit sunt velit. Sit exercitation cillum
        reprehenderit fugiat consequat nisi. Lorem laborum ut occaecat cupidatat
        ex ad sit eu.
      </p>

      <p>
        Ipsum anim exercitation commodo veniam culpa nisi consectetur mollit
        pariatur aliqua fugiat eu incididunt veniam. Dolor labore ea aute eu
        excepteur dolor deserunt exercitation fugiat cillum cillum velit qui.
        Anim exercitation voluptate quis enim laboris non veniam. In nisi ea ea
        proident nisi minim mollit adipisicing sit aute elit. Consectetur ex
        anim nulla exercitation ut. Incididunt officia do ipsum nisi id
        consequat reprehenderit consectetur nisi cupidatat mollit ea. Incididunt
        deserunt proident velit voluptate.
      </p>

      <p>
        Eu veniam sunt cupidatat irure sunt anim voluptate esse sit nisi
        consectetur laborum magna. Duis aute quis fugiat aute esse quis ipsum
        proident nisi cupidatat aliquip sunt. Commodo non dolore sunt est
        consequat.
      </p>
      <Button onClick={handleDisclaimer} style={{width: "80%", margin: "48px auto 0 auto"}} variant="contained" color="primary">
          sunt de acord
        </Button>
    </div>
  );
};

export default DisclaimerBox;
