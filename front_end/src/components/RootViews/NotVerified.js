import { Link } from "react-router-dom";
import "./NotVerified.css";

const NotVerified = () => {

    return (
    <div className="static-dialog-wrapper">
        <img className="dialog-logo" src="/rpb.png"></img>
        <h3>Te-ai conectat cu succes. Profilul tau va fi evaluat de un admin. Dupa verificare vei fi notificat prin e-mail</h3>

        <h5> Pentru testare iti poti activa singur contul urmarind  <br></br> {<Link to="/my-profile">link-ul urmator</Link>}</h5>
    </div>
    )
}

export default NotVerified;