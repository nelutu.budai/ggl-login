import axios from "axios";
import { GoogleLogin } from "react-google-login";
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import "./NotLogged.css";

const axiosApiCall = (url, method, body = {}) =>
  axios({
    method,
    url: `http://localhost:5010${url}`,
    data: body,
  });

const NotLogged = ({ onSignUp }) => {

  const [ termsAccepted, setTermsAccepted] = useState(false);
  const [ gdprAccepted, setGDPRAccepted] = useState(false);
  const [ buttonEnabled, setButtonEnabled] = useState(false);


  const handleTerms = (event)=>{
    setTermsAccepted(event.target.checked)
  }

  const handleGDPR = (event)=>{
    setGDPRAccepted(event.target.checked)
  }

  const onGoogleSuccess = (response) => {
    const access_token = response.accessToken;
    axiosApiCall("/api/auth", "post", { access_token })
      .then((res) => {
        const { user, token } = res.data;
        onSignUp(user._id, token);
      })
      .catch((err) => {
        throw new Error(err);
      });
  };


  const onGoogleFailure = () => {};

  useEffect(()=>{
    setButtonEnabled(termsAccepted&&gdprAccepted?true:false)
  },[termsAccepted, gdprAccepted])

  return (
    
      
        <div className="static-dialog-wrapper">
          <img className="dialog-logo" src="/rpb.png"></img>
          <GoogleLogin
            render={renderProps => (
              <button className="custom-signin" onClick={renderProps.onClick} disabled={!buttonEnabled}>
                <img style={{height: "26px"}} src="/icon-google.png"></img>
                <span style={{textAlign:"center"}}>Conectare cu Google</span>
              </button>
            )}
            clientId="110727163029-pmefc6d6mkptnf9jl1das5tq38okcmur.apps.googleusercontent.com"
            buttonText="Conectare cu Google"
            onSuccess={onGoogleSuccess}
            onFailure={onGoogleFailure}
            cookiePolicy={"http://localhost:3000"}
            className="google-login-button"
          />
          <div className="terms-check-group">
            <FormControlLabel control={<Checkbox style={{color: "#3A63DE"}} checked={termsAccepted} onChange={handleTerms}/>} label={<Link to="/terms" style={{color:"#3A63DE", textDecoration:"none"}}>Accept termenii si conditiile aplicatiei</Link>} />
            <FormControlLabel control={<Checkbox style={{color: "#3A63DE"}} checked={gdprAccepted} onChange={handleGDPR} />} label={<Link to="/gdpr" style={{color:"#3A63DE", textDecoration:"none"}}>Accept politica GDPR a aplicatiei</Link>} />
          </div>

        </div>
  );
};

export default NotLogged;
