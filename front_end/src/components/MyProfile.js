import {
  Icon,
  InputLabel,
  MenuItem,
  FormControl,
  Select,
  Button,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import axios from "axios";

import "./MyProfile.css";

const MyProfile = ({ user, token, onVerify, onRoleAssign }) => {
  const navigate = useNavigate();
  const [userStatus, setUserStatus] = useState(false);
  const [userRole, setUserRole] = useState("nedefinit");

  const handleVerify = async (evt) => {
    const response = await axios({
      method: "patch",
      url: `http://localhost:5010/api/verify-user/${user._id}`,
      data: {
        verified: evt.target.value,
      },
    });
    onVerify(response.data);
  };

  const handleRole = async (evt) => {
    const response = await axios({
      method: "patch",
      url: `http://localhost:5010/api/assign-role/${user._id}`,
      data: {
        role: evt.target.value,
      },
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    onRoleAssign(response.data);
  };

  const handleRedirect = () => {
    navigate('/');
  }

  useEffect(() => {
    if (user._id) {
      setUserStatus(user.verified);
      setUserRole(user.role);
    }
  }, [user]);

  return (
    <>
      <div className="content-header">
        <img className="header-image" src={user.picture}></img>
        {user.name}
        <Icon sx={{ fontSize: 16 }} className="logout-icon">
          logout
        </Icon>
      </div>
      <div className="static-dialog-wrapper">
        <div className="profile-info">
          <img className="profile-picture" src={user.picture}></img>
          <div className="profile-data">
            <h3>{user.name}</h3>
            <h4>{user.email}</h4>
            <h5 className={user.verified ? "text-succes" : "text-danger"}>
              utilizator {user.verified ? "activ" : "in asteptare"}
            </h5>
            <h5
              className={user.role == "nedefinit" ? "text-danger" : "text-info"}
            >
              {user.role == "nedefinit" && "rol "}
              {user.role}
            </h5>
          </div>
        </div>

        <div className="actions-wrapper">
          <FormControl fullWidth>
            <InputLabel id="status-select-label">Stare utilizator</InputLabel>
            <Select
              labelId="status-select-label"
              id="status-select"
              name="status"
              value={userStatus}
              label="Stare utilizator"
              onChange={handleVerify}
            >
              <MenuItem value={false}>in asteptare</MenuItem>
              <MenuItem value={true}>activ</MenuItem>
            </Select>
          </FormControl>

          <FormControl fullWidth>
            <InputLabel id="role-select-label">Rol utilizator</InputLabel>
            <Select
              labelId="role-select-label"
              id="role-select"
              name="role"
              value={userRole}
              label="Rol utilizator"
              onChange={handleRole}
            >
              <MenuItem value={"nedefinit"} disabled={true}>
                nedefinit
              </MenuItem>
              <MenuItem value={"admin"}>admin</MenuItem>
              <MenuItem value={"reviewer"}>reviewer</MenuItem>
              <MenuItem value={"supervizor"}>supervizor</MenuItem>
            </Select>
          </FormControl>
        </div>
        <Button onClick={handleRedirect} style={{width: "80%", margin: "48px auto 0 auto"}} variant="contained" color="primary">
          Mergi mai departe
        </Button>
      </div>
    </>
  );
};

export default MyProfile;
