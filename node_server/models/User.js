const mongoose = require("mongoose")

const schema = mongoose.Schema({
	name: String,
	email: String,
	googleId: String,
	picture: String,
	provider: String,
	verified: Boolean,
	disclaimerReaded: Boolean,
	role: String
})

module.exports = mongoose.model("User", schema)