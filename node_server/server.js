const cors = require("cors");
const express = require("express");
const mongoose = require("mongoose");
const expressJwt = require("express-jwt");
const pathToRegexp = require("path-to-regexp");
const bodyParser = require("body-parser");

const routes = require("./routes");
const app = express();

app.use(cors({ origin: true }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(
  expressJwt({ secret: process.env.JWT_SECRET, algorithms: ["HS256"] }).unless({
    path: [
    pathToRegexp("/api/auth"), 
    pathToRegexp("/api/userById/*"), 
    pathToRegexp("/api/verify-user*"), 
    pathToRegexp("/favicon.ico")],
  })
);
app.use("/api", routes);

mongoose
  .connect(`mongodb://${process.env.MONGO_URL}${process.env.MONGO_DB}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    app.listen(process.env.PORT);
    console.log(`Server is running on port ${process.env.PORT}`);
  })
  .catch((err) => {
    console.log(err);
  });
