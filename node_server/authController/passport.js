const passport = require("passport");
const GoogleTokenStrategy = require("passport-google-token").Strategy;
const User = require("../models/User");

const getProfile = (profile) => {
  const { id, displayName, emails, provider, image, _json } = profile;
  if (emails && emails.length) {
    const email = emails[0].value;
    return { googleId: id, name: displayName, email, provider, picture: _json.picture, verified: false, disclaimerReaded: false, role:"nedefinit" };
  }
  return null;
};

passport.use(
  new GoogleTokenStrategy(
    {
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    },
    //  Passport verify callback
    async (accessToken, refreshToken, profile, done) => {
      try {
        const existingGoogleUser = await User.findOne({ googleId: profile.id });
        if (!existingGoogleUser) {
          const existingEmailUser = await User.findOne({ email: getProfile(profile).email});
          if (!existingEmailUser) {
            const newUser = await User.create(getProfile(profile));
            return done(null, newUser);
          }
          return done(null, existingEmailUser);
        }
        return done(null, existingGoogleUser);
      } catch (e) {
        throw new Error(e);
      }
    }
  )
);

// Saves user's ID to a session
passport.serializeUser((user, done) => {
  done(null, user.id);
});
// Retrieve user's ID from a session
passport.deserializeUser((id, done) => {
  User.findByPk(id).then((user) => {
    done(null, user);
  });
});

module.exports = passport; 
