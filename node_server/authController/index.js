const jwt = require('jsonwebtoken');
const User = require("../models/User");
const AuthController = {
  async googleOauth(req, res) {
    if (!req.user) {
      return res.status(400).send('Authentication failed!');
    }
    const { _id, email } = req.user;
    const user = await User.findOne({ email });
    const token = user.verified ? jwt.sign({ _id, email }, process.env.JWT_SECRET) : null;
    return res.status(200).send({ user, token });
  }
};
module.exports = AuthController;