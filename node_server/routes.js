const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const AuthController = require("./authController");
const passport = require("./authController/passport");

const User = require("./models/User");

router.post(
  "/auth",
  passport.authenticate("google-token", { session: false }),
  AuthController.googleOauth
);

router.patch("/verify-user/:id", async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.params.id });
    const { _id, email } = user;
    user.verified = req.body.verified ? true : false;
    const token = user.verified
      ? jwt.sign({ _id, email }, process.env.JWT_SECRET)
      : null;
    await user.save();
    res.status(200).send({  user, token });
  } catch (err) {
    res.status(404);
    res.send({ error: err });
  }
});

router.patch("/assign-role/:id", async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.params.id });
	user.role = req.body.role;
    await user.save();
    res.status(200).send({  user });
  } catch (err) {
    res.status(404);
    res.send({ error: err });
  }
});


router.patch("/disclaimer-readed/:id", async (req, res) => {
	try {
	  const user = await User.findOne({ _id: req.params.id });
	  user.disclaimerReaded = true;
	  await user.save();
	  res.status(200).send({  user });
	} catch (err) {
	  res.status(404);
	  res.send({ error: err });
	}
  });


router.get("/users", async (req, res) => {
  const users = await User.find();
  res.send(users);
});

router.get("/userById/:id", async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.params.id });
    res.send(user);
  } catch {
    res.status(404);
    res.send({ error: "Utilizator inexistent!" });
  }
});

router.get("/users/:id", async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.params.id });
    res.send(user);
  } catch {
    res.status(404);
    res.send({ error: "Utilizator inexistent!" });
  }
});

router.post("/users", async (req, res) => {
  const post = new User({
    name: req.body.name,
    email: req.body.email,
  });
  await post.save();
  res.send(post);
});

router.patch("/users/:id", async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.params.id });

    if (req.body.name) {
      user.name = req.body.name;
    }

    if (req.body.content) {
      user.email = req.body.email;
    }

    await user.save();
    res.send(user);
  } catch {
    res.status(404);
    res.send({ error: "Utilizator inexistent!" });
  }
});

module.exports = router;
